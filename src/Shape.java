
public abstract class Shape implements TwoDimensionalShapeInterface {
	
	public Shape(String color) {
		this.color = color;
	}

	private String color;
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	//This method should be abstract as the Shape alone is too general to calculate Area
	@Override
	public abstract double calculateArea();
	
	//This method should be abstract as the Shape alone is too general to print info specified in question
	@Override
	public abstract void printInfo();
	
}
