public class Triangle extends Shape {

	public Triangle(String color, double base, double height) {
		super(color);
		this.base = base;
		this.height = height;
	}

	private double base;

	private double height;

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public double calculateArea() {
		return 0.5 * base * height;
	}

	@Override
	public void printInfo() {
		System.out.println("Color: "+ this.getColor());
		System.out.println("Dimensions (base x height) : "+ getBase() +" x "+ getHeight() );
		System.out.println("Area: "+ this.calculateArea());
	}
}
