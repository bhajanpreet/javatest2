import java.util.ArrayList;
import java.util.Scanner;

/**
 * 
 * @author Bhajanpreet Singh (C0748668)
 *
 */
public class Main {

	private static ArrayList<Shape> shapes = new ArrayList<>();

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int choice = 0;

		while (choice != 3) {
			// 1. show the menu
			showMenu();

			// 2. get the user input
			System.out.println("Enter a number: ");
			choice = keyboard.nextInt();

			// 3. DEBUG: Output what the user typed in
			System.out.println("You entered: " + choice);

			// 4. Handle the choice made
			handleChoices(choice);

			System.out.println();
		}
	}

	public static void handleChoices(int choice) {
		Scanner keyboard = new Scanner(System.in);
		if (choice == 1) {
			System.out.println("Please enter the Triangle's color: ");
			String color = keyboard.nextLine().trim();

			System.out.println("Please enter the value of Triangle's base: ");
			double base = keyboard.nextDouble();

			System.out.println("Please enter the value of Triangle's height: ");
			double height = keyboard.nextDouble();
			Shape s = new Triangle(color, base, height);
			// do something here
			System.out.println("*********************************************************");
			System.out.println("The area of this Triangle is: " + s.calculateArea());
			System.out.println("*********************************************************");
			shapes.add(s);
		} else if (choice == 2) {
			System.out.println("Please enter the Square's color: ");
			String color = keyboard.nextLine().trim();

			System.out.println("Please enter the value of Square's side: ");
			double side = keyboard.nextDouble();
			Shape s = new Square(color, side);
			System.out.println("*********************************************************");
			System.out.println("The area of this Square is: " + s.calculateArea());
			System.out.println("*********************************************************");
			shapes.add(s);
		} else if (choice == 3) {
			
			System.out.println("\n*********************************************************");
			System.out.println("******************Displaying Details*********************");
			System.out.println("*********************************************************");
			int i = 0;
			for(Shape shape : shapes) {
				System.out.println("\n++++++++++++++++++++ ( Shape "+ (++i) +" )++++++++++++++++++++++++++");
				shape.printInfo();
				System.out.println("---------------------------------------------------------\n");
			}
			System.out.println("Exiting...");
			System.exit(0);
		} else {
			System.out.println("Enter a valid choice, Try Again....");
		}
	}

	public static void showMenu() {
		System.out.println("AREA GENERATOR");
		System.out.println("==============");
		System.out.println("1. Triangle");
		System.out.println("2. Square");
		System.out.println("3. Exit");
	}

}
