
public class Square extends Shape {
	
	public Square(String color, double side) {
		super(color);
		this.side = side;
	}
	
	private double side;
	
	public double getSide() {
		return side;
	}

	public void setSide(double side) {
		this.side = side;
	}

	@Override
	public double calculateArea() {
		return Math.pow(side, 2);
	}

	@Override
	public void printInfo() {
		System.out.println("Color: "+ this.getColor());
		System.out.println("Dimensions (side) : "+ getSide() );
		System.out.println("Area: "+ this.calculateArea());
	}
}
